const http = require('http');
const keyevents = require('key-events');

const servername = 'localhost';
const port = 3000;

const server = http.createServer((req, res) => {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/plain');
    res.end('Olá mundo!\n');
}).listen(port);

console.log('Servidor rodando em http://'+servername+':'+port);
 
// const puppeteer = require('puppeteer');

// (async () => {
//   const browser = await puppeteer.launch({
//       headless: false,
//   });
//   const page = await browser.newPage();
//   await page.goto('https://pallotti.com.br');
//   await page.screenshot({path: 'pallotti.com.br.png'});

//   //await browser.close();
// })();

// var Jimp = require('jimp');
 
// // open a file called "lenna.png"
// Jimp.read('pallotti.com.br.png', (err, imagem) => {
//   if (err) throw err;
//   imagem
//     .resize(256, 256) // resize
//     .quality(60) // set JPEG quality
//     .greyscale() // set greyscale
//     .write('imagem-small-bw.jpg'); // save
// });